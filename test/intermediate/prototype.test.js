describe('prototype', () => {
  test('Foo', () => {
    class Foo {}

    const foo = new Foo();

    expect(Object.getPrototypeOf(foo)).toEqual(Foo.prototype);

    expect(foo.constructor).toEqual(Foo);
  });

  test('literal', () => {
    const literal = {};

    expect(Object.getPrototypeOf(literal)).toEqual(Object.prototype);

    expect(literal.constructor).toEqual(Object);
  });

  test('Object', () => {
    expect(Object.constructor.prototype).toEqual(Function.prototype);

    expect(Object.constructor).toEqual(Function);
  });


  test('Function', () => {
    expect(Function.constructor.prototype).toEqual(Function.prototype);

    expect(Function.constructor).toEqual(Function);
  });
});
