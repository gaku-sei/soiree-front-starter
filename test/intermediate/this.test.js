describe('this', () => {
  test('bind', () => {
    function returnThis() {
      return this;
    }

    const f = null;

    expect(f({ foo: 42 })).toEqual({ foo: 42 });
  });

  test('call', () => {
    function add(x, y) {
      return x + y;
    }

    expect(add.call()).toBe(42);
  });

  test('apply', () => {
    function add(x, y) {
      return x + y;
    }

    expect(add.apply()).toBe(42);
  });

  test('function', () => {
    function autoCallInternalWith1(x) {
      this.x = x;

      function autoCalled(y) {
        return this.x + y;
      }

      return autoCalled(1);
    };

    // Récurant dans jQuery notamment
    const f = autoCallInternalWith1.bind({});

    // expect(f(2)).toEqual();
  });

  test('arrow function', () => {
    function autoCallInternalWith1(x) {
      this.x = x;

      const autoCalled = (y) => {
        return this.x + y;
      }

      return autoCalled(1);
    };

    // Récurant dans jQuery notamment
    const f = autoCallInternalWith1.bind({});

    // expect(f(2)).toEqual();
  });
});
