const CatClass = require('../../src/class');
const CatConstructor = require('../../src/constructor');

[CatConstructor, CatClass].forEach(Cat => {
  test('Cat', () => {
    const cat = new Cat('Minou', 'roux');
    const cat2 = new Cat('Minette', 'noisette');
    const cat3 = new Cat('Mistigri', 'roux');
  
    expect(cat).toBeInstanceOf(Cat);

    expect(cat.sayHello).toBe(Cat.prototype.sayHello);
  
    expect(cat.sayHello()).toBe('Hello, my name is Minou');
  
    expect(cat.sameColor(cat2)).toBe(false);
    expect(cat.sameColor(cat3)).toBe(true);
  });
});
