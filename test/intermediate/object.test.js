const { f, map, object } = require('../../src/object');

describe('object', () => {
  test('attributes x and y', () => {
    expect(object.x).toBe(1);
    expect(object['y']).toBe('coucou');
  });

  test('method f', () => {
    expect(object.f(1, 2)).toBe(3);
  });

  test('method g', () => {
    expect(object.g(2)).toBe(3);
  });

  // Méthodes héritées de Object
  test('prototype', () => {
    expect(object.hasOwnProperty('x')).toBe(true);
    expect(object.toString()).toBe('[object Object]');
  });
});

describe('map', () => {
  it('should content a foobar key that equals 42', () => {
    expect(map.get('foobar')).toBe(42);
  });
});

describe('this binding', () => {
  expect(f(2)).toBe(3);
});
