const { createNewPost, getAllUserNames } = require('../../src/communication');

describe('Communication', () => {
  test('getAllUserNames', () => {
    expect(getAllUserNames()).resolves.toEqual([
      "Leanne Graham",
      "Ervin Howell",
      "Clementine Bauch",
      "Patricia Lebsack",
      "Chelsey Dietrich",
      "Mrs. Dennis Schulist",
      "Kurtis Weissnat",
      "Nicholas Runolfsdottir V",
      "Glenna Reichert",
      "Clementina DuBuque",
    ]);
  });

  test('createNewPost', () => {
    expect(createNewPost()).resolves.toEqual({
      id: 101,
      title: 'foo',
      body: 'bar',
      userId: 1,
    });
  });
});
