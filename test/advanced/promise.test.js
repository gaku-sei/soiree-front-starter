const { divAsync, rejectedPromise, resolvedPromise } = require('../../src/async');

describe('Promise', () => {
  test('divAsync', () => {
    expect(divAsync(40, 2)).resolves.toBe(20);

    expect(divAsync(40, 0)).rejects.toBeDefined();
  });

  test('resolvedPromise', () => {
    expect(resolvedPromise).resolves.toBe(42);
  });

  test('rejectedPromise', () => {
    expect(rejectedPromise).rejects.toBeDefined();
  });
});
