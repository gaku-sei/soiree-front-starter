test('== vs ===', () => {
  expect(1 == 1)
    // .toBe(true);
    // .toBe(false);

  expect(1 === 1)
    // .toBe(true);
    // .toBe(false);

    expect(1 === '1')
    // .toBe(true);
    // .toBe(false);

  expect(1 == '1')
    // .toBe(true);
    // .toBe(false);

  expect(0 === null)
    // .toBe(true);
    // .toBe(false);

  expect(0 == null)
    // .toBe(true);
    // .toBe(false);
});
