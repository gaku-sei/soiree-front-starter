test('truthiness', () => {
  expect(true)
    // .toBeTruthy();
    // .toBeFalsy();

  expect(false)
    // .toBeTruthy();
    // .toBeFalsy();

  expect(1)
    // .toBeTruthy();
    // .toBeFalsy();

  expect([1])
    // .toBeTruthy();
    // .toBeFalsy();

  expect('coucou')
    // .toBeTruthy();
    // .toBeFalsy();

  expect(null)
    // .toBeTruthy();
    // .toBeFalsy();

  expect('')
    // .toBeTruthy();
    // .toBeFalsy();

  expect([])
    // .toBeTruthy();
    // .toBeFalsy();

  expect(0)
    // .toBeTruthy();
    // .toBeFalsy();

  expect(NaN)
    // .toBeTruthy();
    // .toBeFalsy();

  expect(undefined)
    // .toBeTruthy();
    // .toBeFalsy();

  expect({})
    // .toBeTruthy();
    // .toBeFalsy();
});
