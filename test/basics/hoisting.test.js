describe('var', () => {
  test('hoisting', () => {
    // expect(i).toBe();

    for (var i = 0; i < 10; i++);

    // expect(i).toBe();
  });
});

describe('function', () => {
  test('hoisting', () => {
    // expect(noop).toBeInstanceOf();

    function noop() {}
  });
});

describe('let/const', () => {
  test('lack of hoisting', () => {
    // expect(() => {
    //   expect(i).toBe(undefined);
    // }).toThrow(ReferenceError);

    for (let i = 0; i < 10; i++);

    // expect(() => {
    //   expect(i).toBe(10);
    // }).toThrow(ReferenceError);
  });

  test('let vs const', () => {
    let x = 0;
    const y = 0;

    x = 1;

    // expect(x).toBe();

    // expect(() => {
    //   y = 1;
    // }).toThrow(TypeError);
  });
});
