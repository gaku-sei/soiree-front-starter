test('value vs reference', () => {
  expect(1 === 1)
    // .toBe(true);
    // .toBe(false);

  expect([] === [])
    // .toBe(true);
    // .toBe(false);

  expect('coucou' === 'coucou')
    // .toBe(true);
    // .toBe(false);

  expect({ foo: 1 } === { foo: 1 })
    // .toBe(true);
    // .toBe(false);

  expect({} === {})
    // .toBe(true);
    // .toBe(false);
});
